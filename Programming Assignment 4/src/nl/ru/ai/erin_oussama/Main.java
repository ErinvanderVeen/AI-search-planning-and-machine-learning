package nl.ru.ai.erin_oussama;

import nl.ru.ai.vroon.mdp.*;

/**
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 *
 */
public class Main {
    public static void main(String[] args) {
        MarkovDecisionProblem mdp = new MarkovDecisionProblem();

        // VALUE ITERATION
        ValueIteration vi = new ValueIteration(mdp, 0.9, 0.01);
        Action[][] actions = vi.run();

        while (!mdp.isTerminated()) {
            mdp.performAction(actions[mdp.getStateXPosition()][mdp.getStateYPosition()]);
        }

        mdp.restart();

        // QLEARNING
        QLearning ql = new QLearning(mdp, 0.9, 0.1);

        // Run 100000 iterations to learn the 'optimal' policy using an epsilon of
        // 0.2 meaning that it has a 20% chance for exploration.
        mdp.setShowProgress(false);
        mdp.setWaittime(0);
        ql.run(100000, 0.2);

        // Visually show the agent reaching the terminal state using the learned
        // policy.
        mdp.setShowProgress(true);
        mdp.setWaittime(1000);
        ql.run(1, 1);
    }
}
