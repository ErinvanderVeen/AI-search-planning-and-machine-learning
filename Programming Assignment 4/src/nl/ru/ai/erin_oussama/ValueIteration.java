package nl.ru.ai.erin_oussama;

import java.util.ArrayList;
import nl.ru.ai.vroon.mdp.*;

/**
 * The ValueIteration Class
 * Given a (optional) Markov Decision problem, runs the value iteration
 * algorithm.
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class ValueIteration {
    Action[] possibleActions = Action.values();
    MarkovDecisionProblem problem;
    double discount, delta; // gamma, delta
    double[][][] Q; // x, y, action_nr
    double[][] V; // x, y

    // Constructor 1: Give a problem, discount and a delta
    public ValueIteration(MarkovDecisionProblem problem, double discount, double delta) {
        this.problem = problem;
        this.discount = discount;
        this.delta = delta;
    }

    // Constructor 2: If you want to use the default problem
    public ValueIteration(double discount, double delta) {
        this(new MarkovDecisionProblem(), discount, delta);
    }

    // Constructor 3: If you want to use the default problem and values
    public ValueIteration() {
        this(1.0, 0.1);
    }

    // The actual algorithm
    public Action[][] run() {
        // Init V. All zeros except for the terminating states
        initV();
        // Init Q with all zeros
        initQ();
        // We only continue if one of the states has changed by more then delta
        boolean cont;
        // The new value is stored seperately first to calculate delta
        double newV;

        do {
            // We assume we must not continue
            cont = false;

            // Calculate the Q values for every state and action
            for (int x = 0; x < Q.length; x++) {
                for (int y = 0; y < Q[x].length; y++) {
                    for (int z = 0; z < Q[x][y].length; z++) {
                        Q[x][y][z] = calcNewQ(x, y, z);
                    }
                }
            }

            // Update all V values
            for (int x = 0; x < V.length; x++) {
                for (int y = 0; y < V[x].length; y++) {
                    // Pick the maximum Q
                    newV = calcNewV(x, y);
                    // See if it's bigger then delta
                    cont = Math.abs(newV - V[x][y]) > delta || cont;
                    // Assign the new V value
                    V[x][y] = newV;
                }
            }
        } while(cont);

        // Return the optimal policy
        return calcPi();
    }

    // Init V with all zeros except fot the terminating states
    private void initV() {
        V = new double[problem.getWidth()][problem.getHeight()];
        for (int x = 0; x < problem.getWidth(); x++) {
            for (int y = 0; y < problem.getHeight(); y++) {
                switch (problem.getField(x, y)) {
                    case REWARD:
                        V[x][y] = problem.getPosReward();
                        break;
                    case NEGREWARD:
                        V[x][y] = problem.getNegReward();
                        break;
                    default:
                        V[x][y] = 0;
                        break;
                }
            }
        }
    }

    // Init an empty array of proper size
    private void initQ() {
        Q = new double[problem.getWidth()][problem.getHeight()][Action.values().length];
    }

    // Pick the action that has the best Q value
    private Action[][] calcPi() {
        Action[][] pi = new Action[problem.getWidth()][problem.getHeight()];

        for (int x = 0; x < pi.length; x++) {
            for (int y = 0; y < pi[x].length; y++) {
                pi[x][y] = maxQ(x, y);
            }
        }
        return pi;
    }

    // Calculating Q_k+1(s,a)
    private double calcNewQ(int x, int y, int z) {
        // Get all probabilities
        double pPerform = problem.getpPerform();
        double pSidestep = problem.getpSidestep();
        double pBackstep = problem.getpBackstep();
        double pNoStep = problem.getpNoStep();
        Action action = Action.values()[z];

        // Initiate a new list for the temporary values
        ArrayList<Double> sum = new ArrayList();
        Action a;

        if (!problem.canPerform(x, y, action))
            pNoStep += pPerform; // If we can't go straight, the change of remaining stationary increases.
        else
            sum.add(QSum(pPerform, getNewX(x, action), getNewY(y, action)));
        if (!problem.canPerform(x, y, (a = Action.nextAction(action))))
            pNoStep += pSidestep / 2;
        else
            sum.add(QSum(pSidestep / 2, getNewX(x, a), getNewY(y, a)));
        if (!problem.canPerform(x, y, (a = Action.previousAction(action))))
            pNoStep += pSidestep / 2;
        else
            sum.add(QSum(pSidestep / 2, getNewX(x, a), getNewY(y, a)));
        if (!problem.canPerform(x, y, (a = Action.backAction(action))))
            pNoStep += pBackstep;
        else
            sum.add(QSum(pBackstep, getNewX(x, a), getNewY(y, a)));

        sum.add(QSum(pNoStep, x, y));

        // Add every value in the sum
        double total = 0;
        for (double s : sum) {
            total += s;
        }
        return total;
    }

    // The calculation of one element of the sum in the Q formula
    private double QSum(double factor, int x, int y) {
        return factor *
               (problem.getRewardFromState(x, y)
             + discount * V[x][y]);
    }

    // Given an action return the new X
    private int getNewX(int x, Action action) {
        switch (action) {
            case LEFT:
                x--;
                break;
            case RIGHT:
                x++;
                break;
            default:
                break;
        }
        return x;
    }

    // Given an action return the new Y
    private int getNewY(int y, Action action) {
        switch (action) {
            case UP:
                y++;
                break;
            case DOWN:
                y--;
                break;
            default:
                break;
        }
        return y;
    }

    // To make sure we do not override the V values of the terminating states
    private double calcNewV(int x, int y) {
        switch (problem.getField(x, y)) {
            case EMPTY:
                return maxQVal(x, y);
            default:
                return problem.getPosReward();
        }
    }

    // Return the value of the best action
    private double maxQVal(int x, int y) {
        double max = Q[x][y][0];
        for (int i = 1; i < Q[x][y].length; i++)
            max = Math.max(max, Q[x][y][i]);
        return max;
    }

    // Return the best action
    private Action maxQ(int x, int y) {
        int max = 0;
        for (int i = 1; i < Q[x][y].length; i++)
            max = Q[x][y][max] > Q[x][y][i] ? max : i;
        return Action.values()[max];
    }
}
