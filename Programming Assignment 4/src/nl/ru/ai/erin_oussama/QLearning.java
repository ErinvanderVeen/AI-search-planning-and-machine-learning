package nl.ru.ai.erin_oussama;

import nl.ru.ai.vroon.mdp.*;

/**
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class QLearning {
    private final MarkovDecisionProblem mdp;
    private final double discount, step_size;
    private double Q[][][];
    private final double INIT_VAL = 0;

    public QLearning() {
        this(0.9, 0.1);
    }

    public QLearning(double discount, double step_size) {
        this(new MarkovDecisionProblem(), discount, step_size);
    }

    public QLearning(MarkovDecisionProblem mdp, double discount, double step_size) {
        this.mdp = mdp;
        this.discount = discount;
        this.step_size = step_size;

        // Initialize QTable with every cell INIT_VAL
        Q = new double[mdp.getWidth()][mdp.getHeight()][Action.values().length];
        for (int x = 0; x < mdp.getWidth(); x++)
            for (int y = 0; y < mdp.getHeight(); y++)
                for (int a = 0; a < Action.values().length; a++)
                    Q[x][y][a] = INIT_VAL;
    }

    public void run(int iterations, double epsilon) {
        for (int i = 0; i < iterations; i++) {
            while (!mdp.isTerminated()) {
                // With epsilon percent chance we choose the best action. This
                // allows us to randomly explore instead of exploit.
                if (Math.random() < (1 - epsilon)) {
                    int x = mdp.getStateXPosition();
                    int y = mdp.getStateYPosition();

                    // Get the index with the largest Q value
                    int actionIndex = 0;
                    for (int a = 1; a < Action.values().length; a++)
                        actionIndex = Q[x][y][a] > Q[x][y][actionIndex] ? a : actionIndex;

                    doActionAndUpdate(actionIndex);
                } else {
                    doActionAndUpdate((int) (Math.random() * Action.values().length));
                }
            }

            // During Q-Learning it is possible we hit a terminal state and thus
            // we go back to the initial state.
            mdp.restart();
        }
    }

    private void doActionAndUpdate(int action_index) {
        // Keep track of the previous position so we can use it to update the Q value
        int prevX = mdp.getStateXPosition();
        int prevY = mdp.getStateYPosition();

        double r = mdp.performAction(Action.values()[action_index]);

        int newX = mdp.getStateXPosition();
        int newY = mdp.getStateYPosition();

        // The book gives two variants of the Q update which are equivalent.
        // We will be using the second variant here as it is slightly more intuitive.
        // Q[s, a] <- (1 - alpha) * Q[s, a] + alpha * (r + gamma * max_a' Q[s', a'])

        // Get the best Q value in the new state (max_a' Q[s', a']).
        double maxQValue = Q[newX][newY][0];
        for (int a = 1; a < Action.values().length; a++)
            maxQValue = Q[newX][newY][a] > maxQValue ? Q[newX][newY][a] : maxQValue;

        // Calculate: (1 - alpha) * Q[s, a]
        double firstPart = (1 - step_size) * Q[prevX][prevY][action_index];

        // Calculate: alpha * (r + gamma * max_a' Q[s', a'])
        double secondPart = step_size * (r + discount * maxQValue);

        Q[prevX][prevY][action_index] = firstPart + secondPart;
    }
}
