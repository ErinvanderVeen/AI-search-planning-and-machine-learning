import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Scanner;

/**
 * @author	Erin van der Veen <erin@erinvanderveen.nl>
 * @author	Oussama Danba <oussamadanba@gmail.com>
 * @version	2.1
 */
final class Counter {
	public static Integer c = 0;
}

class Vertex {
	private final Integer id;
	private Boolean inTree = false;
	ArrayList<Edge> edges = new ArrayList<>();

	/**
	 * Vertex constructor for human readability.
	 *
	 * @param id The id of the Vertex, used only when printing the result of the algorithm.
	 * @return Returns the created Vertex.
	 */
	public Vertex(Integer id) {
		this.id = id;
	}
	
	/**
	 * Adds the vertex to the tree by setting the inTree Boolean.
	 *
	 * @return List of edges connected to the vertex.
	 */
	public ArrayList<Edge> addAndReturnFrontier() {
		inTree = true;
		return edges;
	}

	/**
	 * Returns whether or not the vertex is currently in the tree. Used to determine
	 * if a edge can be added.
	 *
	 * @return Returns whether or not the vertex is currently in the tree. 
	 */
	public Boolean inTree() {
		return inTree;
	}

	/**
	 * Gets the id of the vertex, used for human readability.
	 *
	 * @return Returns the id of the vertex.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Adds given edge to the vertex.
	 *
	 * @param e The edge that is connected to the vertex.
	 */
	public void addEdge(Edge e) {
		edges.add(e);
	}
}

class Edge implements Comparable<Edge> {
	private final Integer weight;
	private final Vertex[] vertices = new Vertex[2];
	private Boolean inTree = false;

	/**
	 * Edge constructor, creates an edge given 2 vertices (in no particular order) and a weight.
	 *
	 * @param from One of the vertices.
	 * @param to The second vertex.
	 * @return Returns the created edge.
	 */
	public Edge(Vertex from, Vertex to, int weight){
		from.addEdge(this);
		to.addEdge(this);
		vertices[0] = from;
		vertices[1] = to;
		this.weight = weight;
	}

	/**
	 * Add the edge to the Tree;
	 */
	public ArrayList<Edge> addAndReturnFrontier() {
		if(canAdd()) {
			inTree = true;
			if(vertices[0].inTree())
				return vertices[1].addAndReturnFrontier();
			return vertices[0].addAndReturnFrontier();
		} else {
			return new ArrayList<>();
		}
	}

	/**
	 * Get weight of the edge. Used in the priority queue's comperator.
	 *
	 * @return Returns the weight of the edge.
	 */
	public Integer getWeight() {
		return weight;
	}

	/**
	 * Can we add the edge to the priority queue?
	 *
	 * @return Returns true if either of the endpoints is not in the tree and the edge itself is also not in the tree. 
	 */
	public Boolean canAdd() {
		return (!vertices[0].inTree() || !vertices[1].inTree()) && !inTree();
	}

	/**
	 * Is the edge in the Tree?
	 *
	 * @return Returns true if the edge is in the tree. False otherwise.
	 */
	public Boolean inTree() {
		Counter.c++;
		return inTree;
	}

	/**
	 * String representation of the edge.
	 *
	 * @return Returns String representation of the edge.
	 */
	@Override
	public String toString() {
		return vertices[0].getId() + " | " + vertices[1].getId();
	}

	@Override
	public int compareTo(Edge e) {
		return this.weight.compareTo(e.getWeight());
	}
}

public class Prim {
	public static void main(String[] args) {
		// Used in the creation of the priority queues and ArrayLists
		Integer nrOfEdges, nrOfVertices;

		// Used for storage and reference of the vertices
		ArrayList<Vertex> vertexList = new ArrayList<>();

		// Used for storage and reference of the edges
		ArrayList<Edge> edgeList = new ArrayList<>();

		// Read the number of vertices and edges from the console
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter the number of vertices followed by the number of edges:");
		nrOfVertices = reader.nextInt();
		nrOfEdges = reader.nextInt();

		// Create |V| vertices
		for(int i = 0; i < nrOfVertices; i++)
			vertexList.add(new Vertex(i));

		// Read the edges from console and create them
		System.out.println("Enter the edges by specifying the two vertices followed by the weight:");
		for(int i = 0; i < nrOfEdges; i++)
			edgeList.add(new Edge(vertexList.get(reader.nextInt()), vertexList.get(reader.nextInt()), reader.nextInt()));
		
		// Priority queue to hold the edges for the algorithm, sorted from lowest
		// weight to highest
		PriorityQueue<Edge> edgePQ = new PriorityQueue<>(edgeList.size());
		
		// Run Prim's algorithm
		edgePQ.addAll(vertexList.get((new Random()).nextInt(vertexList.size())).addAndReturnFrontier());
		while(!edgePQ.isEmpty()) {
			for(Edge e : edgePQ.poll().addAndReturnFrontier()) {
				if(e.canAdd())
					edgePQ.add(e);
			}
		}

		// Display result
		for(Edge e : edgeList) {
			if(e.inTree())
				System.out.println(e.toString());
		}

		// Display counter
		System.out.println("Done in " + Counter.c + " evaluations of edges.");
	}
}

