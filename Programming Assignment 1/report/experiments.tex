\subsection{Increasing $|V|$ in G}
\label{sub:increasing_v_in_g}
How does the runtime change for increasing number of vertices $|V|$ in G?
	\subsubsection{Hypothesis}
	\label{ssub:hypothesis1}
	Our hypothesis is that it should not change the amount of evaluations at all since our implementation is $\mathcal{O}(|E|log|E|)$ and since we don't change the amount of edges the amount of evaluations is constant. The actual time the algorithm needs will change since there is overhead outside of Prim's algorithm such as allocating more memory.

	\subsubsection{Results}
	\label{ssub:results1}
	Considering we only want to see the effect of vertices and more vertices means more edges so we will attempt to mitigate this factor by constructing a random connected graph with 100 vertices and 4500 edges as our base case. Every time we add vertices we will keep the same amount of edges so that they will not be a factor. For these test cases we no longer randomly choose the first vertex so that testing will be more consistent. Generating the random connected graphs was done by a python program written by Wesley Baugh\cite{baugh_16}. Our test cases give the following graph:\\
	\begin{tikzpicture}
		\begin{axis}[
			title={Evaluation dependence on Vertices},
			xlabel={Vertices},
			ylabel={Evaluations},
			scaled ticks=false,
			tick label style={/pgf/number format/fixed},
			xmin=0, xmax=3200,
			ymin=9000, ymax=12500,
			xtick={0, 500, 1000, 1500, 2000, 2500, 3000, 3500},
			ytick={9000, 9500, 10000, 10500, 11000, 11500, 12000, 12500},
			ymajorgrids=true,
			grid style=dashed
		]
		\addplot[mark=square] coordinates{(100, 9016)(200, 9153)(400, 9376)(800, 9786)(1600, 10590)(3200, 12196)};
		\end{axis}
	\end{tikzpicture}\\
	The results of our test cases do in fact show that the amount of evaluations is dependent on the amount of vertices. However this can be attributed to our testing method. Because the amount of edges has to be kept the same the graph becomes less dense when we add more vertices. We start out with 100 vertices and 4500 edges which means on average a vertex has 45 edges. Our last test case has 3200 vertices and 4500 edges which means on average a vertex has around 1.4 edges. As a result is it much more likely that a vertex has only one edge to another vertex which means that we have to evaluate a lot more edges in a sparse tree than we have to do in a dense tree. With these test cases we're basically comparing the best case (a dense graph) with the worst case (a sparse graph). Thus we think this test cannot be accurately performed without changing the amount of edges as well.

\subsection{Increasing $|E|$ for different $|V|$}
\label{sub:increasing_e_for_different_v}
How does the runtime change if the number of edges $|E|$ increases from $|V|-1$ to $|V| \cdot \frac{|V|-1}{2}$ for different $|V|$?
	\subsubsection{Hypothesis}
	\label{ssub:hypothesis2}
	Since the runtime complexity of the algorithm is $\mathcal{O}(|E|log|E|)$. We expect that changing the number of edges also changes the runtime of the algorithm.
	\subsubsection{Results}
	\label{ssub:results2}
	\begin{tikzpicture}
		\begin{axis}[
			title={Evaluation dependence on number of edges},
			xlabel={Edges},
			ylabel={Evaluations},
			scaled ticks=false,
			tick label style={/pgf/number format/fixed},
			xmin=0, xmax=20000,
			ymin=0, ymax=40000,
			xtick={0, 4000, 8000, 12000, 16000, 20000},
			ytick={0, 4000, 8000, 12000, 16000, 20000, 24000, 28000, 32000, 36000, 40000},
			ymajorgrids=true,
			grid style=dashed,
			legend style ={ at={(1.3,1)}, 
			anchor=north west, draw=black, 
			fill=white,align=left},
			cycle list name=black white,
			smooth
		]
		\addplot[mark=square] coordinates{(49, 136)(637, 1305)(1225, 2485)};
		\addlegendentry{$|V| = 50$}
		\addplot coordinates{(99, 258)(2524, 5105)(4950, 9925)};
		\addlegendentry{$|V| = 100$}
		\addplot[mark=triangle] coordinates{(149, 403)(5662, 11416)(11175, 22382)};
		\addlegendentry{$|V| = 150$}
		\addplot[mark=diamond] coordinates{(199, 534)(10049, 20201)(19900, 39967)};
		\addlegendentry{$|V| = 200$}
		\end{axis}
	\end{tikzpicture}
	As we expected, we can see an increase in the amount of evaluations when we increase the amount of edges. It looks linear, but since the numbers are quite low, it's hard to differentiate $\mathcal{O}(|E|log|E|)$ from $\mathcal{O}(|E|)$.

\subsection{Runtime in relation to number of equal edge-weights}
\label{sub:runtime_in_relation_to_number_of_equal_edge_weights}
Does the runtime change in relation to the amount of equal edge-weights?
	\subsubsection{Hypothesis}
	\label{ssub:hypothesis3}
	We are not expecting the amount of evaluations to change based on the amount of equal edge-weights. Suppose we have a random connected graph with only weights 0. In our algorithm this would mean the priority queue will only have edges with weight 0 and is thus randomly selecting edges to add. Now suppose we have a random connected graph with all different weights. This is equivalent to having all random weights. This again means that the algorithm will be randomly selecting edges to add and would thus be the same as a graph with only weights 0.

	\subsubsection{Results}
	\label{ssub:results3}
	The test cases we used here use a random connected graph with 1000 vertices and 2000 edges. The first test case only has edges with weight 0. The second test case has half the edges with weight 0 and half the edges with with weight 1. The third test case has half the edges with weight 0, a quarter of the edges with weight 1 and another quarter with weight 2 and so on until we're at the 9th test case. After that we used the graph with 2000 different weights. Just like the other experiments the random start was removed to produce consistent results. \textit{All} test cases resulted in 4995 evaluations leading us to conclude that our hypothesis was right and that the amount of equal edge-weight is irrelevant to the amount of evaluations.

\subsection{Runtime in relation to size of edge-weights}
\label{sub:runtime_in_relation_to_size_of_edge_weights}
Does the runtime change in relation to size of edge-weights?
	\subsubsection{Hypothesis}
	\label{ssub:hypothesis4}
	Just like experiment 3 we are not expecting any difference in the amount of evaluations. Changing the size of the weights does not change the order in which edges will be considered because relative to each other the edges are still the same. If an edge had a weight higher than the other edge it will still have a higher weight than the other edge if the weights are scaled up. Even in the actual running time of the program we are not expecting any difference since a modern computer will not do anything differently regardless if the weight is 1 or one million.

	\subsubsection{Results}
	\label{ssub:results4}
	Our hypothesis is indeed confirmed by our test cases. We took a random connected graph with 250 vertices and 30000 edges and ran it twice through our implementation. Once with all edges having a weight of 0 and once with all edges having a weight of 1000000. Both runs give us 60010 evaluations.

\subsection{Runtime in relation to range of edge-weights}
\label{sub:runtime_in_relation_to_range_of_edge_weights}
Does the runtime change in relation to the range of edge-weights?
	\subsubsection{Hypothesis}
	\label{ssub:hypothesis5}
	We do not think the range of the edge-weights will influence the amount of evaluations. This is much like experiment 4 where the computing time of high or small numbers is virtually the same on modern computers. Neither do we think it matters if the weight is higher than the amount of vertices because weight and the amount of vertices are unrelated in our implementation.

	\subsubsection{Results}
	\label{ssub:results5}
	The same test cases as experiment 4 can be used because the test cases in experiment 4 already cover the test cases in this experiment. Our hypothesis is indeed correct. What we did not cover is the case that a weight is infinite but that is something we cannot test since our implementation uses integers in Java which do not have a representation of infinite. Java does have Integer.MAX\_VALUE but that is actually $2^{31}-1$ which would be the same as just using a large number.
