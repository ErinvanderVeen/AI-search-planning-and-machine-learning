package varelim;

/**
 * Contains a single variable. Together with all the information that is needed
 * elsewhere in the program.
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class Variable {
    private final String name;
    private final String[] possibleVals;
    private String value;

    /**
     * Constructor of the class.
     *
     * @param name
     * @param possibleValsStr
     */
    public Variable(String name, String possibleValsStr) {
        this.name = name;
        this.possibleVals = possibleValsStr.replaceAll("\\s", "").split(",");
    }

    /**
     * Getter of the name.
     *
     * @return the name of the variable.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter of the value.
     *
     * @return the value of the variable.
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter of the value.
     *
     * @param value To which the value of the variable should be set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Getter of the possible values
     *
     * @return A list with all possible string values of this variable
     */
    public String[] getPossibleVals() {
        return possibleVals;
    }
}
