package varelim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class ContainedInMostFactorsElimOrder implements EliminationOrder {
    @Override
    public List<Variable> getEliminationOrder(Network network, List<Factor> factors) {
        // Sort the variables in the network based on in how many factors
        // the variables are present in.
        Collections.sort(network.getVars(), (Variable o1, Variable o2) ->
                containedInNFactors(o1, factors) - containedInNFactors(o2, factors));

        // Copy the variables in a separate list so that later methods can modify
        // the original list without messing up the elimination ordering.
        List<Variable> order = new ArrayList<>();
        for (Variable var : network.getVars()) {
            order.add(var);
        }

        // Remove the queried variable from the elimination ordering
        order.remove(network.getQueriedVar());

        // Reverse the list so variables contained in more factors appear first
        Collections.reverse(order);
        return order;
    }

    private int containedInNFactors(Variable var, List<Factor> factors) {
        int counter = 0;
        for (Factor factor : factors) {
            if (factor.getVars().contains(var)) {
                counter += 1;
            }
        }
        return counter;
    }

    @Override
    public String getName() {
        return "Contained in most factors first";
    }
}
