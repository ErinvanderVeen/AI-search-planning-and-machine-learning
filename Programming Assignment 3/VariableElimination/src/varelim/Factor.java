package varelim;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * The factor class that implements the multiplication and removal of variables
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class Factor {
    private List<FactorElem> elements;
    private List<Variable> vars;

    public Factor(List<Variable> vars, List<Probability> probs) {
        this.vars = vars;
        this.elements = new ArrayList<>();

        for (Probability p : probs) {
            elements.addAll(p.toFactorElem());
        }
    }

    // The boolean "hoi" is a hack. It's designed to work around a java
    // "feature" called type erasure.
    public Factor(List<FactorElem> elements, List<Variable> vars, boolean hoi) {
        this.vars = vars;
        this.elements = elements;
    }

    public Factor multiply(Factor f) {
        List<Variable> vs = new ArrayList<>(vars);
        List<FactorElem> newElems = new ArrayList<>();
        List<Variable> newVars = new ArrayList<>(vars);

        vs.retainAll(f.getVars());
        if (vs.size() != 1) {
            // The two factors must contain exactly one shared variable
            return null;
        }

        Variable redundant = vs.get(0);
        newVars.remove(redundant);
        newVars.addAll(f.getVars());

        for (String s : vs.get(0).getPossibleVals()) {
            for (FactorElem fe1 : elements) {
                for (FactorElem fe2 : f.getElements()) {
                    if (Arrays.asList(fe1.getVals()).get(vars.indexOf(redundant)).equals(s)
                            && Arrays.asList(fe2.getVals()).get(f.getVars().indexOf(redundant)).equals(s)) {
                        newElems.add(fe1.multiply(fe2, s));
                        // Update multiply counter
                        VariableElimination.MULTIPLY_COUNT += 1;
                    }
                }
            }
        }

        return new Factor(newElems, newVars, true);
    }

    public void observe(Variable v) {
        for (FactorElem fe : elements) {
            if(vars.indexOf(v) != -1 && !Arrays.asList(fe.getVals()).get(vars.indexOf(v)).equals(v.getValue())) {
                fe.markForDeletion();
            }
        }

        elements.removeIf(p -> p.shouldBeDeleted());
    }

    public List<Variable> getVars() {
        return vars;
    }

    public List<FactorElem> getElements() {
        return elements;
    }

    public void sum(Variable v) {
        for (Iterator<FactorElem> it = elements.iterator(); it.hasNext();) {
            FactorElem fe = it.next();
            ArrayList<String> newVals = new ArrayList(Arrays.asList(fe.getVals()));
            newVals.remove(vars.indexOf(v));
            fe.setVals(newVals.toArray(new String[0]));
        }

        for (FactorElem fe1 : elements) {
            if (fe1.shouldBeDeleted())
                continue;
            for (FactorElem fe2 : elements) {
                if (fe2.shouldBeDeleted())
                    continue;
                if (Arrays.asList(fe1.getVals()).equals(Arrays.asList(fe2.getVals()))
                        && fe1 != fe2) {
                    fe2.markForDeletion();
                    fe1.setProbability(fe1.getProbability() + fe2.getProbability());

                    // Update addition counter.
                    VariableElimination.ADDITION_COUNT += 1;
                }
            }
        }

        vars.remove(v);
        elements.removeIf(p -> p.shouldBeDeleted());
    }

    public void normalize() {
        // Calculate the sum of all elements
        double sum = 0;
        for (FactorElem fe : elements)
            sum += fe.getProbability();

        for (FactorElem fe : elements) {
            fe.setProbability(fe.getProbability() / sum);
        }
    }

    @Override
    public String toString() {
        String output = "";
        for (Variable var : vars) {
            output += var.getName() + "\t";
        }
        output += "\n";

        for (FactorElem fe : elements) {
            output += fe.toString() + "\n";
        }

        return output;
    }

    public int getSize() {
        return vars.size();
    }
}
