package varelim;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Class that reads in a network and puts the variables and probabilities at the
 * right places.
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */

public class Network {
    private final List<Variable> vars;
    private final List<ProbabilityDistribution> probs;
    private final List<Variable> observedVars;
    private Variable queriedVar;

    /**
     *
     * @param file The name of the .bif file that contains the network.
     */
    public Network(String file) {
        String fileContent = null;
        vars = new ArrayList<>();
        probs = new ArrayList<>();
        observedVars = new ArrayList<>();

        try {
            // Get a list of the lines of the files
            List<String> lines = Files.readAllLines(Paths.get(file),
                    StandardCharsets.US_ASCII);

            // Flatten the list into a single string
            fileContent = String.join("\n", lines);
        } catch (InvalidPathException e) {
            System.err.println("Invallid path specified: "
                    + e.getLocalizedMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println("FileIOException: " + e.getLocalizedMessage());
            System.exit(-1);
        } catch (SecurityException e) {
            System.err.println("No acces to file: " + e.getLocalizedMessage());
            System.exit(1);
        }

        readVariables(fileContent);
        readProbabilities(fileContent);
    }

    private void readVariables(String fileContent) {
        String regex = "variable\\s+(.*?)\\s*\\{\\s*type discrete\\s*"
                + "\\[\\s*\\d+\\s*\\]\\s*\\{\\s*(.*?)\\s*\\};\\s*\\}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(fileContent);

        while(matcher.find()) {
            vars.add(new Variable(matcher.group(1), matcher.group(2)));
        }
    }

    private void readProbabilities(String fileContent) {
        String noParentRegex = "probability\\s*\\(\\s*(.*?)\\s*\\)\\s*"
                + "\\{\\s*table\\s+(.*?);\\s*\\}";
        String withParentRegex = "probability\\s*\\(\\s*(.*?)\\s*\\|"
                + "\\s*(.*?)\\s*\\)\\s*\\{\\s*((?:.|\\n)*?)\\}";

        Pattern noParentPattern = Pattern.compile(noParentRegex);
        Pattern withParentPattern = Pattern.compile(withParentRegex);

        Matcher noParentM = noParentPattern.matcher(fileContent);
        Matcher withParentM = withParentPattern.matcher(fileContent);

        while (noParentM.find()) {
            Variable child = (Variable) vars.stream().filter(p -> p.getName()
                    .equals(noParentM.group(1))).toArray()[0];

            // Since there are no parents the list will simply be empty.
            // While this could be null it is easier to work with an empty list
            // when choosing an elimination ordering.
            probs.add(new ProbabilityDistribution(noParentM.group(2),
                    child.getPossibleVals(), child, new ArrayList<>()));
        }

        while (withParentM.find()) {
            Variable child = (Variable) vars.stream().filter(p -> p.getName()
                    .equals(withParentM.group(1))).toArray()[0];

            String[] parentNames = withParentM.group(2).replaceAll("\\s", "").split(",");

            // Go from parent names to parent Variable objects
            List<Variable> parentVariables = new ArrayList<>();
            for (String parentName : parentNames) {
                parentVariables.add((Variable) vars.stream()
                        .filter(p -> p.getName().equals(parentName)).toArray()[0]);
            }

            probs.add(new ProbabilityDistribution(withParentM.group(3), child.getPossibleVals(), child, parentVariables));
        }
    }

    /**
     * Method to ask for a query from the user.
     */
    public void askForQuery() {
        System.out.println("\nWhich variable do you want to query? Please enter in the number of the variable.");
        printVariables();

        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        if (line.isEmpty()) {
            System.out.println("No variable queried. Please enter a query value.");
            askForQuery();
        }
        try {
            int queriedVarIndex = Integer.parseInt(line);
            if (queriedVarIndex >= 0 && queriedVarIndex < vars.size()) {
                queriedVar = vars.get(queriedVarIndex);
            } else {
                System.err.println("This is not a correct index."
                        + " Please choose an index between " + 0 +
                        " and " + (vars.size() - 1) + ".");
                askForQuery();
            }
        } catch (NumberFormatException ex) {
            System.err.println("This is not a correct index."
                    + " Please choose an index between " + 0 +
                    " and " + (vars.size() - 1) + ".");
            askForQuery();
        }
    }

    /**
     * Method to ask the user for observed variables in the network.
     */
    public void askForObservedVariables() {
        observedVars.clear();
        System.out.println("Which variable(s) do you want to observe? Please enter in the number of the variable,\n"
                        + "followed by a comma and the value of the observed variable. Do not use spaces.\n"
                        + "If you want to query multiple variables, delimit them with a ';' and no spaces.\n"
                        + "Note that the values are case sensitive!\n"
                        + "Example: '2,Equal;3,Severe'");
        for(int i = 0; i < vars.size(); i++)
            System.out.println("Variable " + i + ": " + vars.get(i).getName());

        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        if (!line.isEmpty()) {
            if (!line.contains(",")) {
                    System.out.println("You did not enter a comma between values. Please try again");
                    askForObservedVariables();
            } else {
                // Multiple observed variables
                while (line.contains(";")) {
                    try {
                        int queriedVarIndex = Integer.parseInt(line.substring(0, line.indexOf(",")));
                        String value = line.substring(line.indexOf(",") + 1, line.indexOf(";"));
                        changeVariableToObserved(queriedVarIndex, value);
                        line = line.substring(line.indexOf(";") + 1);
                    } catch (NumberFormatException ex) {
                        System.out.println("This is not a correct input."
                                + "Please choose an index between " + 0 +
                                " and " + (vars.size() - 1) + ".");
                        askForObservedVariables();
                        return;
                    }
                }
                // Only one observed variable
                if (!line.contains(";")) {
                    try {
                        int queriedVarIndex = Integer.parseInt(line.substring(0, line.indexOf(",")));
                        String value = line.substring(line.indexOf(",") + 1);
                        changeVariableToObserved(queriedVarIndex, value);
                    } catch (NumberFormatException ex) {
                        System.out.println("This is not a correct input."
                                + "Please choose an index between " + 0 +
                                " and " + (vars.size() - 1) + ".");
                        askForObservedVariables();
                    }
                }
            }
        }
    }

    /**
     * Method that checks whether input is valid or not and then adds it to the observed list.
     * @param queriedVar The variable that is queried
     * @param value The value of the requested variable
     */
    public void changeVariableToObserved(int queriedVar, String value) {
        Variable ob;
        if (queriedVar >= 0 && queriedVar < vars.size()) {
            ob = vars.get(queriedVar);
            if (Arrays.asList(ob.getPossibleVals()).contains(value)) {
                ob.setValue(value);
            } else {
                System.out.println("Apparently you did not fill in a valid"
                        + " Value for the variable. Try again");
                askForObservedVariables();
                return;
            }
            observedVars.add(ob);
        } else {
            System.out.println("You have chosen an incorrect index."
                    + " Please choose an index between " + 0 +
                    " and " + (vars.size() - 1));
            askForObservedVariables();
        }
    }

    /**
     * Method to print the network that was read-in (by printing the variables, parents and probabilities).
     */
    public void printNetwork() {
        System.out.println("The network has the following properties:\nThe variables:");
        printVariables();

        System.out.println("\nThe probabilities:");
        for (ProbabilityDistribution probDist : probs) {
            if (probDist.getParents() == null || probDist.getParents().isEmpty()) {
                System.out.println(probDist.getChild().getName() + " has no parents.");
            } else {
                for (Variable parent : probDist.getParents())
                    System.out.println(probDist.getChild().getName() + " has parent " + parent.getName());
            }

            // Print probabilties corresponding to variable
            for (Probability probability : probDist.getProbabilities())
                System.out.println("Probability given " + probability);
        }
    }

    /**
     * Method to print the query and observed variables given in by the user.
     */
    public void printQueryAndObserved() {
        System.out.println("\nThe queried variable is: " );
        System.out.println(queriedVar.getName());
        if (!observedVars.isEmpty()) {
            System.out.println("The observed variable(s) is/are: ");
            for (Variable observedVar : observedVars) {
                System.out.println(observedVar.getName() +
                        " = " + observedVar.getValue());
            }
        }
    }

    public void printVariables() {
        for (int i = 0; i < vars.size(); i++)
            System.out.println(i + ") " + vars.get(i).getName());
    }

    public List<ProbabilityDistribution> getProbs() {
        return probs;
    }

    public List<Variable> getVars() {
        return vars;
    }

    public List<Variable> getObservedVars() {
        return observedVars;
    }

    public Variable getQueriedVar() {
        return queriedVar;
    }
}
