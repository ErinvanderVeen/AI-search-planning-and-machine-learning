package varelim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class VariableElimination {
    private final Network network;
    private List<Variable> eliminationOrder;
    private final List<EliminationOrder> possibleOrders;
    private ArrayList<Factor> factors;
    public static int MULTIPLY_COUNT = 0;
    public static int ADDITION_COUNT = 0;

    public VariableElimination(Network network) {
        this.network = network;
        possibleOrders = new ArrayList<>();
        possibleOrders.add(new ManualElimOrder());
        possibleOrders.add(new LeastIncomingArcsElimOrder());
        possibleOrders.add(new MostIncomingArcsElimOrder());
        possibleOrders.add(new ContainedInFewestFactorsElimOrder());
        possibleOrders.add(new ContainedInMostFactorsElimOrder());
    }

    public Factor run() {
        List<ProbabilityDistribution> probs = network.getProbs();
        factors = new ArrayList<>();

        for (ProbabilityDistribution p : probs) {
            factors.add(p.toFactor());
        }

        for (Variable v : network.getObservedVars()) {
            for (Factor f : factors) {
                // Only attempt to observe if a variable is actually
                // present in a factor.
                if (f.getVars().contains(v)) {
                    f.observe(v);
                }
            }
        }

        System.out.println("The largest factor has a size of: " + largestFactorSize() + "\n");
        queryEliminationOrder(factors);

        for (Variable v : eliminationOrder) {
            for (int i = 0; i < factors.size(); i++) {
                for (int j = 0; j < factors.size(); j++) {
                    if (j != i && factors.get(i).getVars().contains(v) &&
                            factors.get(j).getVars().contains(v)) {
                        Factor newFactor = factors.get(i).multiply(factors.get(j));
                        if (newFactor != null) {
                            // Temporarily save in variables since the list
                            // indices change after deleting the first factor.
                            // By saving them in variables we use Object removal
                            // instead of index removal.
                            Factor removal1 = factors.get(i);
                            Factor removal2 = factors.get(j);

                            factors.remove(removal1);
                            factors.remove(removal2);
                            factors.add(newFactor);

                            // Restart loops as there might be multiple
                            // factors with the same variable
                            i = 0;
                            j = 0;
                        }
                    }
                }
            }

            for (Factor f : factors) {
                if (f.getVars().contains(v)) {
                    f.sum(v);
                }
            }
        }

        while (factors.size() > 1) {
            Factor newFactor = factors.get(0).multiply(factors.get(1));
            Factor removal1 = factors.get(0);
            Factor removal2 = factors.get(1);

            factors.remove(removal1);
            factors.remove(removal2);
            factors.add(newFactor);
        }

        // Normalize the values so that we get probabilities in the range of 0 to 1.
        factors.get(0).normalize();

        return factors.get(0);
    }

    private void queryEliminationOrder(List<Factor> factors) {
        System.out.println("What kind of elimination order would you like to use?");
        for (int i = 0; i < possibleOrders.size(); i++) {
            System.out.println(i + ") " + possibleOrders.get(i).getName());
        }

        Scanner scan = new Scanner(System.in);
        String line = scan.nextLine();
        int choice = 0;

        try {
            choice = Integer.parseInt(line);
        } catch (NumberFormatException ex) {
            System.err.println("Incorrect number format: " + ex.getLocalizedMessage());
            queryEliminationOrder(factors);
            return;
        }
        if (choice >= 0 && choice < possibleOrders.size())
            eliminationOrder = possibleOrders.get(choice).getEliminationOrder(network, factors);
        else
            queryEliminationOrder(factors);
    }

    private int largestFactorSize() {
        return Collections.max(factors, (Factor f1, Factor f2) -> f1.getSize() - f2.getSize()).getSize();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Network network = new Network("test/alarm.bif");
        network.printNetwork();

        network.askForQuery();
        network.askForObservedVariables();
        network.printQueryAndObserved();

        VariableElimination varelim = new VariableElimination(network);
        Factor resultingFactor = varelim.run();

        System.out.println("\nThe results are:");
        System.out.println("Variable " + resultingFactor.toString());
        System.out.println("There were " + MULTIPLY_COUNT +
                " multiplication steps and " + ADDITION_COUNT + " addition steps.");
    }
}
