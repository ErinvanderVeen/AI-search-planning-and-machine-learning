package varelim;

import java.util.List;

/**
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public interface EliminationOrder {
    public List<Variable> getEliminationOrder(Network network, List<Factor> factors);
    public String getName();
}
