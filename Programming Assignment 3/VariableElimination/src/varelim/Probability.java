package varelim;

import java.util.ArrayList;

/**
 * A single probability. The class contains a single probability that can
 * be found in a probability distribution
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class Probability {
    private final String[] parentVals;
    private final double[] probabilities;
    private final String[] possibleVals;

    /**
     * Constructor
     *
     * @param s the string directly from the file
     * @param possibleVals the values that the variable can take
     * @param child
     */
    public Probability(String s, String[] possibleVals, Variable child) {
        String parValsStr;
        String probsStr;
        String[] probsStrArr;

        if (s.contains(")")) {
            parValsStr = s.split("\\)")[0];
            probsStr = s.split("\\)")[1];
            parValsStr = parValsStr.replaceAll("\\s|\\(", "");
            this.parentVals = parValsStr.split(",");
        } else {
            probsStr = s;
            this.parentVals = null;
        }

        probsStr = probsStr.replaceAll("\\s|;", "");
        probsStrArr = probsStr.split(",");

        this.probabilities = new double[probsStrArr.length];
        this.possibleVals = possibleVals;

        for (int i = 0; i < probsStrArr.length; i++) {
            try {
                this.probabilities[i] = Double.parseDouble(probsStrArr[i]);
            } catch (NumberFormatException e) {
                System.err.println("Could not parse number in .bif file: "
                        + e.getLocalizedMessage());
                System.exit(1);
            }
        }
    }

    /**
     * Getter of the probabilities.
     *
     * @return the probabilities of the variable
     */
    public double[] getProbabilities() {
        return probabilities;
    }

    /**
     * Getter for the parentVals
     *
     * @return the parent values of this instance
     */
    public String[] getParentVals() {
        return parentVals;
    }

    public ArrayList<FactorElem> toFactorElem() {
        ArrayList<FactorElem> fes = new ArrayList<>();

        for (int i = 0; i < possibleVals.length; i++) {
            String[] s;
            if (parentVals != null) {
                s = new String[parentVals.length + 1];
                System.arraycopy(parentVals, 0, s, 0, parentVals.length);
                s[parentVals.length] = possibleVals[i];
            } else {
                s = new String[1];
                s[0] = possibleVals[i];
            }

            fes.add(new FactorElem(s, probabilities[i]));
        }
        return fes;
    }

    @Override
    public String toString() {
        String output = "";

        if (parentVals != null) {
            for (String parentVal : parentVals) {
                output += parentVal + " ";
            }
        }

        output += ": ";
        for (double probability : probabilities)
            output += probability + " ";

        return output;
    }
}
