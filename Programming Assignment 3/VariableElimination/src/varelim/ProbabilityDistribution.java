package varelim;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Erin van der Veen
 * @author Oussama Danba
 */
public class ProbabilityDistribution {
    private final List<Probability> probs;
    private final Variable child;
    private final List<Variable> parents;

    /**
     * Create the distribution from the input string
     *
     * @param probDistString
     * @param possibleVals
     * @param child
     * @param parents
     */
    public ProbabilityDistribution(String probDistString, String[] possibleVals,
            Variable child, List<Variable> parents) {
        this.parents = parents;
        this.child = child;
        this.probs = new ArrayList<>();

        String[] probArray = probDistString.replaceAll("\n", "").split(";");

        for (String s : probArray) {
            probs.add(new Probability(s, possibleVals, child));
        }
    }

    /**
     * Convert the distribution to a factor
     *
     * @return the created factor
     */
    public Factor toFactor() {
        ArrayList<Variable> vars = new ArrayList<>(parents);
        vars.add(child);
        return new Factor(vars, probs);
    }

    public Variable getChild() {
        return this.child;
    }

    public List<Variable> getParents() {
        return this.parents;
    }

    public List<Probability> getProbabilities()  {
        return this.probs;
    }
}
