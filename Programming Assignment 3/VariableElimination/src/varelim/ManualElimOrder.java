package varelim;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class ManualElimOrder implements EliminationOrder {
    @Override
    public List<Variable> getEliminationOrder(Network network, List<Factor> factors) {
        boolean error;
        network.printVariables();
        List<Variable> possibleVars = network.getVars();
        ArrayList<Variable> order = new ArrayList<>();

        do {
            error = false;
            System.out.println("What is the elimination order that you would like?");
            System.out.println("Please enter them seperated by commas, e.g. 5,3,2");
            System.out.println("Note that the query variable should not be in here!");

            Scanner scan = new Scanner(System.in);
            String line = scan.nextLine();

            List<String> vars = Arrays.asList(line.split(","));

            for (String var : vars) {
                int index;
                try {
                    index = Integer.parseInt(var);
                    order.add(possibleVars.get(index));
                } catch (NumberFormatException ex) {
                    System.err.println("Incorrect input: " + ex.getLocalizedMessage());
                    error = true;
                    break;
                }
            }
        } while (error);
        return order;
    }

    @Override
    public String getName() {
        return "Manual";
    }
}
