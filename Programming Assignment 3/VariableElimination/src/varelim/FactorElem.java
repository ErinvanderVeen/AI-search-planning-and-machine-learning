package varelim;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Contains a single entry in a Factor
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class FactorElem {
    private boolean toBeDeleted = false;
    private String[] vals;
    private double probability;

    /**
     *
     * @param vals
     * @param probability
     */
    public FactorElem(String[] vals, double probability) {
        this.vals = vals;
        this.probability = probability;
    }

    public FactorElem multiply(FactorElem fe, String var) {
        double newProbability = probability * fe.getProbability();

        ArrayList<String> valsArr1 = new ArrayList(Arrays.asList(vals));
        ArrayList<String> valsArr2 = new ArrayList(Arrays.asList(fe.getVals()));

        valsArr1.remove(var);
        valsArr1.addAll(valsArr2);

        return new FactorElem(valsArr1.toArray(new String[0]), newProbability);
    }

    public String[] getVals() {
        return vals;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double p) {
        probability = p;
    }

    public void markForDeletion() {
        toBeDeleted = true;
    }

    public boolean shouldBeDeleted() {
        return toBeDeleted;
    }

    void setVals(String[] vals) {
        this.vals = vals;
    }
    
    @Override
    public String toString() {
        String output = "";
        for (String s : vals) {
            output += s + " ";
        }
        
        output += ": " + probability;
        
        return output;
    }
}
