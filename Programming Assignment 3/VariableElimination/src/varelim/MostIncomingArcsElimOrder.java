package varelim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Erin van der Veen (s4431200)
 * @author Oussama Danba (s4435591)
 */
public class MostIncomingArcsElimOrder implements EliminationOrder {
    @Override
    public List<Variable> getEliminationOrder(Network network, List<Factor> factors) {
        // Sort the probability distributions in the network on size of parent
        // list. As a result variables with less parents will be earlier in the list.
        Collections.sort(network.getProbs(),
                (ProbabilityDistribution pb1, ProbabilityDistribution pb2) ->
                        pb1.getParents().size() - pb2.getParents().size());

        // Get all the variables from the probability distributions and put
        // them in a separate list so we can return them.
        List<Variable> order = new ArrayList<>();
        for (ProbabilityDistribution pb : network.getProbs()) {
            order.add(pb.getChild());
        }

        // Remove the queried variable from the elimination ordering
        order.remove(network.getQueriedVar());

        // Reverse the list so variables with more parents appear first
        Collections.reverse(order);
        return order;
    }

    @Override
    public String getName() {
        return "Most incoming arcs first";
    }
}
